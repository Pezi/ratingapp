import { Pipe, PipeTransform } from '@angular/core';
import { Company } from '../components/company';

@Pipe({ name: 'filterCompanies' })
export class FilterCompaniesPipe implements PipeTransform {
    public transform(companies: Company[], filter: string): any[] {
//    public transform(values: any[], filter: string): any[] {
        if (!companies || !companies.length) { return []; }
        if (!filter) { return companies; }
        // Filter items array, items which match will return true
        return companies.filter(v => v.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
    }
}
