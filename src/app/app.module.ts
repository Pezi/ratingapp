import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

// server
import { HttpModule } from '@angular/http';
import { ServerModule } from '@angular/platform-server';

// routing
// import { RouterModule, Routes} from '@angular/router';
import { AppRoutingModule, routedComponents } from './app-routing.module';

// other modules
import { InfiniteScrollModule } from 'angular2-infinite-scroll';

 import { LoadingModule } from 'ngx-loading';

// own app
import { AppComponent } from './app.component';
import { RatingListComponent } from './components/rating-list/rating-list.component';
import { DataService } from './services/data.service';
import { FilterCompaniesPipe } from './pipes/filter-companies.pipe';
import { EinliefererPortalComponent } from './components/einlieferer-portal/einlieferer-portal.component';


@NgModule({
    declarations: [
        AppComponent,
        RatingListComponent,
        FilterCompaniesPipe,
        EinliefererPortalComponent
    ],
    imports: [
        BrowserModule.withServerTransition({appId: 'ang4-seo-pre'}),
        FormsModule,
        HttpModule,
        InfiniteScrollModule,
        AppRoutingModule,
        LoadingModule
    ],
    providers: [DataService],
    bootstrap: [AppComponent]
})
export class AppModule { }
