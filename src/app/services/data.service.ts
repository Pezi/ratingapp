import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

    constructor(public http:Http) {
        console.log("dataservice connected");
    }

    getCompanyData(url){
        // console.log(url);
        return this.http.get(url)
            .map(res => res.json());
    };
}
