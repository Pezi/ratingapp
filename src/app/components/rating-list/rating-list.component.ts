import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

import { DataService } from '../../services/data.service';
import { Company } from '../company';

@Component({
    selector: 'app-rating-list',  // muss evtl leer sein, wenn routing gehen soll
    templateUrl: './rating-list.component.html',
    styleUrls: ['./rating-list.component.css']
})

export class RatingListComponent implements OnInit {
    companies: Company[];
    companiesTemp: Company[] = [];
    companyNameList: Company[] = [];
    boolLoadMore = false;
    companyReloadSize = 20;
    reloadPage = 0;
    search: string = null;
    maxResults = 10;
    boolValidSearch = false;
    public boolLoading = true;
    boolAppInit = false;

    constructor(private dataService: DataService, meta: Meta, title: Title) {
        title.setTitle('Rating List - Home');

        meta.addTags([
            { name: 'author',   content: 'Peter'},
            { name: 'keywords', content: 'profiforms, rating, companies'},
            { name: 'description', content: 'this page shows the rating from different companies' }
        ]);
        console.log('rating list constructor ran');
    }

    ngOnInit() {
        // boolAppInit ist zur indizierung da, dass die App gestartet wurde. damit beim route-wechsel die daten nicht noch einmal neu geholt werden. Muss global gemacht werden
        if (!this.boolAppInit) {
            this.getCompanyRatings();
            this.getCompanyNames();
            this.boolAppInit = true;
        }
    }

// Funktionen
    getCompanyRatings() {
        // URL MUSS eine komplette Adresse sein, sonst wirft der ng2 server ein fehler
        const url: string = 'http://localhost:8080/companies?page=' + this.reloadPage + '&size=100';
//        const url: string = 'http://localhost:8080/companies?page=' + this.reloadPage + '&size=' + this.companyReloadSize;
        // getPosts returns "Observeble" - man muss subscribe dann benutzen
        this.dataService.getCompanyData(url).subscribe(
            (companies) => {
                // beim appstart ist this.companies noch undefined, somit kann kein concat befehl ausgeführt werden.
                if (typeof(this.companies) !== 'undefined') {
                    this.companies = this.companies.concat(companies._embedded.Company);
                } else {
                    this.companies = companies._embedded.Company;
                }
                this.reloadPage ++;
                this.boolLoading = false;
                // console.log(companies._embedded.Company);
            },
            err => this.logError(err));
    }

    toggleLoading() {
        this.boolLoading = !this.boolLoading;
    }

    logError(err: any) {
        console.warn(err);
    }

    getCompanyNames() {
        const url: string = 'http://localhost:8080/companies/search/getAllCompanies';
        this.dataService.getCompanyData(url).subscribe(
            (names) => {
                /* if (typeof(names._embedded.Company) !== 'undefined') {
                    names._embedded.Company.forEach(function (value) {
                        console.log(value);
                        this.companyNames.concat(value);
                    });
                } */

                this.companyNameList = names._embedded.Company;
                // console.log(this.nameList);
            },
            err => this.logError(err));

    }

    // Suchfeld leeren
    reset() {
        if (this.boolValidSearch) {
            this.companies = this.companiesTemp;
            this.companiesTemp = [];
            this.boolValidSearch = false;
        }
        this.search = '';
        // console.log("reset - clear searchfield");
    }

    // Sternrating "directive" um die sterne anhand der bewertung auszufüllen
    getClass(index, rating) {
        // half star: http://fontawesome.io/icon/star-half/
        if (index < rating) {
            return 'glyphicon-star';
        } else {
           return 'glyphicon-star-empty gray';
        }
    }

    searchCompany() {
        if (this.search) {
            this.boolLoading = true;

            const url = 'http://localhost:8080/companies/search/findByNameIgnoreCaseContaining?name=' + this.search + '&size=' + this.maxResults;

            // hat suche ein ergebnis gebracht
            // this.companies in temp variablen speichern, damit die komplett geladenen daten nicht noch einmal geladen werden müssen
                // -> weniger server requests
            // ergebnis in this.companies speichern
            // loadMore auf false setzen, damit beim scrollen kein neuer request erzeugt wird, welcher dann nach dem reset verloren geht.
            // flag setzen, dass die suche erfolgreich war
            // reset button überprüfen, ob eine suche erfolg gebracht hat
                // temp variable wieder auf this.companies setzen und inpul leeren
                // erbrachte suche kein ergebnis nur suchfeld leeren
            // TODO: evtl noch this.reloadPage zwischen speichern und dann auch wieder resetten, falls doch nach der suche ein scroll-load
                // ausgeführt wurde, sonst gehen die daten verloren

            this.dataService.getCompanyData(url).subscribe((companies) => {
                if (Object.keys(this.companiesTemp).length === 0) {
                    this.companiesTemp = this.companies;
                }
                this.companies = companies._embedded.Company;
                this.boolValidSearch = true;
                this.boolLoadMore = false;
                this.boolLoading = false;
                // console.log(this.companiesTemp, this.companies);
            });

            console.log('SUCHE: ', this.search);
        } else {
            alert('bitte eine Eingabe in Suchfeld machen');
        }
    }

    loadMore () {
        if (!this.boolLoadMore) {
            // console.log('boolLoadMore is false');
            return;
        }
        this.getCompanyRatings();
        // console.log('scrolled!!');
    }

    toggleLoadMore() {
        // console.log("reset - clear searchfield");
        this.boolLoadMore = !this.boolLoadMore;
    }
}
