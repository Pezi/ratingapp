import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EinliefererPortalComponent } from './einlieferer-portal.component';

describe('EinliefererPortalComponent', () => {
  let component: EinliefererPortalComponent;
  let fixture: ComponentFixture<EinliefererPortalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EinliefererPortalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EinliefererPortalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
