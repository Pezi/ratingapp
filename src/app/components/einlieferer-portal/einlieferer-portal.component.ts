import { Component, OnInit } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';

@Component({
    selector: 'app-einlieferer-portal',
    templateUrl: './einlieferer-portal.component.html',
    styleUrls: ['./einlieferer-portal.component.css']
})
export class EinliefererPortalComponent implements OnInit {
    constructor(meta: Meta, title: Title) {
        title.setTitle('Einlieferer-Portal');

        meta.addTags([
            { name: 'author',   content: 'Peter'},
            { name: 'keywords', content: 'profiforms, IO, stuff'},
            { name: 'description', content: 'Einliefererportal' }
        ]);
    }

    ngOnInit() {
    }

}
