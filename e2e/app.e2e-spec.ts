import { RatingAppPage } from './app.po';

describe('rating-app App', () => {
  let page: RatingAppPage;

  beforeEach(() => {
    page = new RatingAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
